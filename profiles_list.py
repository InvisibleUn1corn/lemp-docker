from backend import database

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk


def delete_profile(widget, event, data):
    if data["profile"] == get_selected_profile(data["listbox"]):
        data["listbox"].unselect_all()

    database.delete_config(data["profile"])
    build(data["listbox"])


def right_click(widget, event, data):
    try:
        if event.button == 3:
            menu = Gtk.Menu()

            # Create rename menu
            menu_item = Gtk.MenuItem("Rename")
            menu.append(menu_item)
            menu_item.show()

            # Create delete menu
            menu_itemb = Gtk.MenuItem("Delete")
            menu.append(menu_itemb)
            menu_itemb.connect('button-press-event', delete_profile, data)
            menu_itemb.show()

            menu.popup_at_widget(widget.get_child(), Gdk.Gravity.NORTH_EAST, Gdk.Gravity.NORTH_WEST, None)
    except Exception as e:
        print("Error while showing right click")


def build(profiles_list):
    profiles = database.get_all_profiles()
    childs = profiles_list.get_children()
    childs_str = []

    for child in childs:
        try:
            childs_str.append(child.get_child().get_child().get_child().get_text())

            if not(child.get_child().get_child().get_child().get_text() in profiles):
                profiles_list.remove(child)
                print("%s REMOVED" % child.get_child().get_child().get_child().get_text())

        except Exception as e:
            print("ERROR UPDATING PROFILES LIST")

    for profile in profiles:
        if not(profile in childs_str):
            row_label = Gtk.Label(profile)
            row_label.set_halign(Gtk.Align.START)
            row_label.set_valign(Gtk.Align.START)
            row_label.set_margin_left(5)

            row = Gtk.ListBoxRow()
            row.add(row_label)

            eventbox = Gtk.EventBox()
            eventbox.connect('button-press-event', right_click, {"profile": profile, "listbox": profiles_list})
            eventbox.add(row)
            profiles_list.add(eventbox)

            print("%s WAS ADDED TO THE PROFILES LIST" % profile)

    profiles_list.show_all()


def get_selected_profile(listbox):
    if listbox.get_selected_row() is None:
        return None

    return listbox.get_selected_row().get_child().get_child().get_child().get_text() # TODO: fix problem when removing selected profile
