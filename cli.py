#!/usr/bin/env python3

import sys
from backend import api, database

if __name__ == "__main__":
    if len(sys.argv) > 2:
        if sys.argv[1] == 'rm':
            api.delete_env(sys.argv[2])
        elif sys.argv[1] == 'start':
            api.create_env(database.get_config(sys.argv[2]))
        elif sys.argv[1] == 'create' and len(sys.argv) > 3:
            database.create_config(sys.argv[2], sys.argv[3])
        elif sys.argv[1] == 'check':
            print(api.is_profile_running(sys.argv[2]))
    elif len(sys.argv) > 1 and sys.argv[1] == 'clean':
        api.clean_env()
