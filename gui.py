#!/usr/bin/env python3

import threading

from backend import api, database
import profiles_list
import settings.php
import settings.nginx
import settings.mysql

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
from gi.repository import Gtk, GObject, Notify, Gdk


class MyApp:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("MyApp.glade")
        self.builder.connect_signals(self)

        self.profiles_listbox = self.builder.get_object("ProfilesList")
        profiles_list.build(self.profiles_listbox)

        self.window = self.builder.get_object("MainWindow")
        self.window.set_size_request(500, 300)
        headerbar = self.builder.get_object("MainHeaderBar")
        self.window.set_titlebar(headerbar)
        self.window.show_all()

    def quit(self, *args):
        try:
            api.clean_env()
            Gtk.main_quit(*args)
        except Exception as e:
            dialog = Gtk.MessageDialog(self.window, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, "Something happaned")
            dialog.format_secondary_text(str(e))
            dialog.run()
            print("ERROR dialog closed")

            dialog.destroy()

    def get_selected_profile(self):
        profiles_listbox = self.builder.get_object("ProfilesList")
        return profiles_list.get_selected_profile(profiles_listbox)

    def profile_changed(self, listbox, row):
        selected_profile = self.get_selected_profile()
        profile_switch = self.builder.get_object("ProjectSwitch")
        profile_running = api.is_profile_running(selected_profile)

        profile_name = self.builder.get_object("ProfileName")
        profile_name.set_text(selected_profile)

        config = database.get_config(selected_profile)
        settings.nginx.build_settings(self.builder, config, profile_running)
        settings.php.build_settings(self.builder, config, profile_running)
        settings.mysql.build_settings(self.builder, config, profile_running)

        profile_switch.set_state(profile_running)

    def on_profile_switch_change(self, widget, state):
        selected_profile = self.get_selected_profile()
        profile_running = api.is_profile_running(selected_profile)

        if not(state == profile_running):
            t = threading.Thread(target=self.toggle_profile, args=(selected_profile, state))
            t.start()

    def toggle_profile(self, profile, state):
        try:
            if state:
                api.create_env(database.get_config(profile))
                GObject.idle_add(settings.nginx.toggle_controls, self.builder, state)
                GObject.idle_add(settings.mysql.toggle_controls, self.builder, state)
                GObject.idle_add(settings.php.toggle_controls, self.builder, state)
            else:
                api.delete_env(profile)
                GObject.idle_add(settings.nginx.toggle_controls, self.builder, state)
                GObject.idle_add(settings.mysql.toggle_controls, self.builder, state)
                GObject.idle_add(settings.php.toggle_controls, self.builder, state)

        except Exception as e:
            GObject.idle_add(self.show_error, e, state)

    def show_error(self, e, state):
        profile_switch = self.builder.get_object("ProjectSwitch")
        profile_switch.set_state(not(state))

        dialog = Gtk.MessageDialog(self.window, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Something happaned")
        dialog.format_secondary_text(str(e))
        dialog.run()
        print("ERROR dialog closed")

        dialog.destroy()

    def open_existing_project(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a folder", self.window,
                                       Gtk.FileChooserAction.SELECT_FOLDER,
                                       (Gtk.STOCK_CANCEL,
                                        Gtk.ResponseType.CANCEL,
                                        "Select", Gtk.ResponseType.OK))

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            path = dialog.get_filename()
            name = path.split('/')[-1]
            database.create_config(name, path)
            profiles_list.build(self.profiles_listbox)
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def open_create_project_form(self, widget):
        pass

    def update_setting(self, widget):
        """
        Listen to changes of the settings and update the relavant setting in
        the database

            widget - the widget that changed, from it we get all the data
        """
        key = widget.get_name()

        if type(widget) is Gtk.ComboBoxText:
            val = widget.get_active_text()
        elif type(widget) is Gtk.SpinButton:
            val = widget.get_value_as_int()
        elif type(widget) is Gtk.CheckButton:
            val = widget.get_active()

        database.update_setting(self.get_selected_profile(), key, val)


app = MyApp()
Gtk.main()
