import os
import pwd
from subprocess import call
from sys import platform

if platform == "linux" or platform == "linux2":
    call(["groupadd", "docker"])
    call(["gpasswd", "-a", pwd.getpwuid(os.getuid())[0], "docker"])
    call(["service", "docker", "restart"])
