CONFIGS_FOLDER = '/MyApp'


def nginx(client, config):
    """
    Create a docker container for nginx and returns the container
    @returns docker.models.containers.Container
    """
    name = "myapp_%s_nginx" % config["PROJECT_NAME"]
    version = 'nginx:%s' % config["NGINX_VERSION"]
    config_path = config["PROJECT_PATH"] + CONFIGS_FOLDER + '/nginx.conf'
    port = str(config["NGINX_PORT"]) + '/tcp'

    # TODO: check if nginx.conf exist and if not copy it

    php_name = "myapp_%s_php" % config["PROJECT_NAME"]
    phpmyadmin_name = "myapp_%s_phpmyadmin" % config["PROJECT_NAME"]

    return client.containers.run(version, name=name, volumes={
        config["PROJECT_PATH"]: {'bind': '/code', 'mode': 'ro'},
        config_path: {'bind': '/etc/nginx/conf.d/default.conf', 'mode': 'ro'}
    }, ports={port: 80}, links={
        php_name: 'php',
        phpmyadmin_name: 'phpmyadmin'
    }, detach=True)


def php(client, config):
    """
    Create a docker container for php and returns the container
    @returns docker.models.containers.Container
    """
    name = 'myapp_%s_php' % config['PROJECT_NAME']
    version = 'php:%s-fpm' % config['PHP_VERSION']

    mysql_name = 'myapp_%s_mysql' % config['PROJECT_NAME']

    container = client.containers.run(version, name=name, volumes={
        config['PROJECT_PATH']: {'bind': '/code', 'mode': 'ro'}
    }, links={mysql_name: 'mysql'}, detach=True)
    container.exec_run('docker-php-ext-install pdo pdo_mysql mysqli')
    container.restart()

    return container


def phpmyadmin(client, config):
    if config['PHPMYADMIN_ENABLED'] is False:
        return None

    name = 'myapp_%s_phpmyadmin' % config['PROJECT_NAME']
    version = 'phpmyadmin/phpmyadmin'

    mysql_name = 'myapp_%s_mysql' % config['PROJECT_NAME']

    return client.containers.run(version, name=name, links={
        mysql_name: 'mysql'
    }, environment={
        'PMA_HOST': 'mysql',
        # 'PMA_ABSOLUTE_URI': 'http://localhost/phpmyadmin'
        'PMA_ABSOLUTE_URI': '/phpmyadmin'
    }, detach=True)


def mysql(client, config):
    """
    Create a docker container for mysql and returns the container
    @returns docker.models.containers.mysql
    """
    name = "myapp_%s_mysql" % config["PROJECT_NAME"]
    version = 'mysql:%s' % config["MYSQL_VERSION"]

    return client.containers.run(version, name=name, environment={
        "MYSQL_ALLOW_EMPTY_PASSWORD": True
    }, detach=True)
