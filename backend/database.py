import os
import sqlite3

SQLITE_TRUE = 1
SQLITE_FALSE = 0

default_config = {
    'PROJECT_NAME': '',
    'PROJECT_PATH': '/',

    'MYSQL_VERSION': '5.7',
    'MYSQL_PORT': 3306,
    'MYSQL_USER': 'root',
    'MYSQL_PASSWORD': '',
    'MYSQL_ALLOW_EMPTY_PASSWORD': True,

    'PHPMYADMIN_ENABLED': SQLITE_TRUE,

    'PHP_VERSION': '7.1',

    'NGINX_VERSION': 'latest',
    'NGINX_PORT': 80
}


class MyAppException(Exception):
    pass


def get_id_by_name(profile):
    profile_id = '0'
    conn = sqlite3.connect('configs.db')
    cursor = conn.execute("SELECT profile_id FROM profiles WHERE name = ? LIMIT 1", (profile, ))
    row = cursor.fetchone()

    if row is None:
        raise MyAppException("Profile %s does not exist" % profile)

    profile_id = row[0]
    return profile_id


def update_setting(profile, key, val):
    conn = sqlite3.connect('configs.db')
    profile_id = get_id_by_name(profile)

    cursor = conn.execute("SELECT val FROM settings WHERE profile_id = ? AND key = ?", (profile_id, key))
    row = cursor.fetchone()

    if row is None:
        cursor = conn.execute("INSERT INTO settings (profile_id, key, val) VALUES (?, ?, ?)", (profile_id, key, val))
    else:
        cursor = conn.execute("UPDATE settings SET val = ? WHERE profile_id = ? AND key = ?", (val, profile_id, key))

    conn.commit()
    conn.close()


def get_all_profiles():
    conn = sqlite3.connect('configs.db')
    cursor = conn.execute("SELECT name FROM profiles")
    rtn_list = []

    for row in cursor:
        if is_config_dir_exist(row[0]):
            rtn_list.append(row[0])

    conn.close()
    return rtn_list


def is_config_dir_exist(profile):
    conn = sqlite3.connect('configs.db')
    cursor = conn.execute("SELECT dir FROM profiles WHERE name = ? LIMIT 1", (profile, ))
    row = cursor.fetchone()

    if row is None:
        raise MyAppException("Profile %s does not exist" % profile)

    return os.path.isdir(row[0])


def get_config(profile):
    conn = sqlite3.connect('configs.db')
    cursor = conn.execute("SELECT name, port, dir FROM profiles WHERE name = ? LIMIT 1", (profile, ))
    row = cursor.fetchone()

    if row is None:
        raise MyAppException("Profile %s does not exist" % profile)

    config = default_config.copy()
    config['PROJECT_NAME'] = row[0]
    config['NGINX_PORT'] = row[1]
    config['PROJECT_PATH'] = row[2]

    profile_id = get_id_by_name(profile)
    cursor = conn.execute("SELECT key, val FROM settings WHERE profile_id = ?", (profile_id, ))
    for row in cursor:
        key = row[0]
        val = row[1]

        if not(key in default_config):
            continue

        if type(default_config[key]) is int:
            config[key] = int(val)
        else:
            config[key] = val

    conn.close()
    return config


def create_config(profile, path):
    if not(os.path.isdir(path)):
        raise MyAppException("Path is not a directory")

    conn = sqlite3.connect('configs.db')
    conn.execute("INSERT INTO profiles (name, dir) VALUES (?, ?)", (profile, path))
    conn.commit()
    conn.close()


def delete_config(profile_name):
    conn = sqlite3.connect('configs.db')
    conn.execute("DELETE FROM profiles WHERE name = ?", (profile_name, ))
    conn.commit()
    conn.close()
