#!/usr/bin/env python3

import docker

from backend import builders

CONFIGS_FOLDER = '/MyApp'


def is_profile_running(profile):
    client = docker.from_env()

    try:
        php_container = client.containers.get('myapp_%s_php' % profile)
        mysql_container = client.containers.get('myapp_%s_mysql' % profile)
        nginx_container = client.containers.get('myapp_%s_nginx' % profile)

        return ((php_container.status == 'running' and mysql_container.status == 'running') and nginx_container.status == 'running')
    except docker.errors.NotFound:
        return False


def create_env(config):
    client = docker.from_env()

    builders.mysql(client, config)
    builders.php(client, config)
    builders.phpmyadmin(client, config)
    builders.nginx(client, config)


def delete_env(profile):
    client = docker.from_env(timeout=60)

    php_container = client.containers.get('myapp_%s_php' % profile)
    mysql_container = client.containers.get('myapp_%s_mysql' % profile)
    myadmin_container = client.containers.get('myapp_%s_phpmyadmin' % profile)
    nginx_container = client.containers.get('myapp_%s_nginx' % profile)

    php_container.stop()
    mysql_container.stop()
    myadmin_container.stop()
    nginx_container.stop()

    php_container.remove()
    mysql_container.remove()
    myadmin_container.remove()
    nginx_container.remove()


def clean_env():
    '''
    clean all docker containers that were created by this program from the env
    '''
    client = docker.from_env()

    for container in client.containers.list(all=True):
        try:
            if container.name.startswith('myapp_'):
                container.stop()
                container.remove()
        except Exception as e:
            print(e)
