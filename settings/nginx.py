from gi.repository import Gtk

MIN_PORT = 1
MAX_PORT = 65535
DEFAULT_PORT = 80


def build_settings(builder, config, running):
    adj = Gtk.Adjustment(DEFAULT_PORT, MIN_PORT, MAX_PORT, 1, 1, 1)
    nginx_port = builder.get_object("NGINX_PORT")
    nginx_port.configure(adj, 1, 0)
    nginx_port.set_value(config["NGINX_PORT"])

    toggle_controls(builder, running)


def toggle_controls(builder, running):
    nginx_port = builder.get_object("NGINX_PORT")

    if running:
        nginx_port.set_sensitive(False)
    else:
        nginx_port.set_sensitive(True)
