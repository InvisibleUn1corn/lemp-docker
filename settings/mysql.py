from gi.repository import Gtk

MIN_PORT = 1
MAX_PORT = 65535
DEFAULT_PORT = 3306


def build_settings(builder, config, running):
    adj = Gtk.Adjustment(DEFAULT_PORT, MIN_PORT, MAX_PORT, 1, 1, 1)
    mysql_port = builder.get_object("MYSQL_PORT")
    mysql_port.configure(adj, 1, 0)
    mysql_port.set_value(config["MYSQL_PORT"])

    mysql_version = builder.get_object("MYSQL_VERSION")

    for i, row in enumerate(mysql_version.get_model()):
        if config["MYSQL_VERSION"] == row[0]:
            mysql_version.set_active(i)

    phpmyadmin = builder.get_object("PHPMYADMIN_ENABLED")
    # FIXME: why is this always 1?
    if config["PHPMYADMIN_ENABLED"] == 1:
        phpmyadmin.set_active(True)
    else:
        phpmyadmin.set_active(False)

    toggle_controls(builder, running)


def toggle_controls(builder, running):
    mysql_port = builder.get_object("MYSQL_PORT")
    mysql_version = builder.get_object("MYSQL_VERSION")
    phpmyadmin = builder.get_object("PHPMYADMIN_ENABLED")

    if running:
        mysql_port.set_sensitive(False)
        mysql_version.set_sensitive(False)
        phpmyadmin.set_sensitive(False)
    else:
        mysql_port.set_sensitive(True)
        mysql_version.set_sensitive(True)
        phpmyadmin.set_sensitive(True)
