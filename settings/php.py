def build_settings(builder, config, running):
    php_version = builder.get_object("PHP_VERSION")

    for i, row in enumerate(php_version.get_model()):
        if config["PHP_VERSION"] == row[0]:
            php_version.set_active(i)

    toggle_controls(builder, running)


def toggle_controls(builder, running):
    php_version = builder.get_object("PHP_VERSION")

    if running:
        php_version.set_sensitive(False)
    else:
        php_version.set_sensitive(True)
