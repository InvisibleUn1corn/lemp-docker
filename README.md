## Installation

You need to install docker's python sdk and run the program as root or add yourself to the docker group

```
sudo pip install docker
./get_access.py
./gui.py
```

## Config Folder

As of now, the PHP project needs to have the conf directory inside of it and be called MyApp
